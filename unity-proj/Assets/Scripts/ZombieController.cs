﻿using UnityEngine;
using System.Collections;

public class ZombieController : MonoBehaviour {

	public float moveSpeed;
	public float turnSpeed;
	private Vector3 moveDirection;

	// Use this for initialization
	void Start () {
		moveDirection = Vector3.right;
	}
	
	// Update is called once per frame
	void Update () {
		//Copy position to local variable
		Vector3 currentPosition = transform.position;
		//Check if 'Fire' button is pressed
		if (Input.GetButton ("Fire1")) {
			//Convert mouse position to a world coordinate
			Vector3 moveToward = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			//Calculate position
			moveDirection = moveToward - currentPosition;
			moveDirection.z = 0;
			moveDirection.Normalize();
		}
		//calculate target position that is moveSpeed units away
		Vector3 target = moveDirection * moveSpeed + currentPosition;
		//Determines new location
		transform.position = Vector3.Lerp (currentPosition, target, Time.deltaTime);
	
		float targetAngle = Mathf.Atan2 (moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
		transform.rotation = 
			Quaternion.Slerp (transform.rotation,
			          			Quaternion.Euler (0, 0, targetAngle),
			                 turnSpeed * Time.deltaTime);
			                
	}
}
